name := "retention-analysis"

version := "1.0"

scalaVersion := "2.11.8"

// Force Scala Version
ivyScala := ivyScala.value map {
  _.copy(overrideScalaVersion = true)
}

// Spark Dependencies
val sparkVersion = "2.3.0"
libraryDependencies += "org.apache.spark" %% "spark-core" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-sql" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-streaming" % sparkVersion
libraryDependencies += "org.apache.spark" %% "spark-hive" % sparkVersion

// Scala Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5"
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5"

// Additional Dependencies
// Typesafe Config
libraryDependencies += "com.typesafe" % "config" % "1.3.2"
// Joda Time
libraryDependencies += "joda-time" % "joda-time" % "2.9.4"
// Elasticsearch
libraryDependencies += "org.elasticsearch" % "elasticsearch-hadoop" % "6.3.1"
// Cron Scheduler
libraryDependencies += "org.quartz-scheduler" % "quartz" % "2.2.1"
// Avro Connector
libraryDependencies += "com.databricks" %% "spark-avro" % "4.0.0"


// Merge Strategy
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}
