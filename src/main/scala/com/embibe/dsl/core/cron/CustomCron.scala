package com.embibe.dsl.core.cron

import com.typesafe.config.Config
import org.quartz.CronExpression._

/** Implementation of Custom Cron Expression
  *
  * Created by Mukul Dev on 7th October 2018.
  */
class CustomCron(override val cronConfig: Config) extends Cron(cronConfig) {
  val optionConfig = cronConfig.getConfig("option")
  override val cronExpression: String = if (optionConfig.hasPath("cron")) {
    val expression = optionConfig.getString("cron")
    if (isValidExpression(expression)) {
      expression
    } else {
      throw new IllegalArgumentException("Custom Cron - Not a valid expression")
    }
  } else {
    throw new IllegalArgumentException("Custom Cron - Expression Not Defined")
  }
}
