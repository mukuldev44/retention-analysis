package com.embibe.dsl.core.cron

import java.util.TimeZone

import com.typesafe.config.Config
import org.joda.time.DateTime
import org.quartz.CronExpression

/** Cron Base Trait
  *
  * Created by Mukul Dev on 7th October 2018.
  */
abstract class Cron(val cronConfig: Config) {
  val cronExpression: String

  /** Get Next Run Time from Reference Time
    *
    * @param referenceTime Time whose next time is to be calculated
    * @return Next Run Time from Reference Time
    */
  def getNextRunTime(referenceTime: DateTime): DateTime = {
    val referenceDateTimeZone = referenceTime.getZone
    val referenceUtilDate = referenceTime.toDate
    val referenceTimeZone = referenceDateTimeZone.toTimeZone
    TimeZone.setDefault(referenceTimeZone)
    val nextUtilDate = new CronExpression(cronExpression).getNextValidTimeAfter(referenceUtilDate)
    if (nextUtilDate == null) {
      throw new IllegalArgumentException("Not a valid cron expression " + cronExpression)
    }
    new DateTime(nextUtilDate.getTime).withZone(referenceDateTimeZone)
  }
}

/** Cron Companion - Used as Factory
  *
  */
object Cron {
  def apply(cronConfig: Config): Cron = {
    val unitType = cronConfig.getString("unit").toLowerCase
    unitType match {
      case "custom" => new CustomCron(cronConfig)
      case _ =>
        throw new IllegalArgumentException(
          "Cron does not support specified type : " + unitType)
    }

  }
}
