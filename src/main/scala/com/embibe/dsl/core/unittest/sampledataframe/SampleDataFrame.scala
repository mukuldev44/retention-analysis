package com.embibe.dsl.core.unittest.sampledataframe

import org.apache.spark.sql.{DataFrame, SQLContext}

/** Return Sample DataFrames
  *
  * Created by Mukul Dev on 8th October 2018.
  */
object SampleDataFrame {

  /** Sample DataFrame 1
    *
    * @param sqlContext SQLContext required for creating context
    * @return Sample DataFrame created at runtime
    */
  def returnSampleDFType1(sqlContext: SQLContext): DataFrame = {
    sqlContext
      .createDataFrame(
        Seq(
          ("0", "Agriculture"),
          ("1", "Mining"),
          ("2", "Construction"),
          ("3", "Manufacturing"),
          ("4", "Transportation"),
          ("5", "Wholesale Trade"),
          ("6", "Retail Trade"),
          ("7", "Finance"),
          ("8", "Services"),
          ("9", "Public Administration")
        ))
      .toDF("code", "industry")
  }

  /** Sample DataFrame 2
    *
    * @param sqlContext SQLContext required for creating context
    * @return Sample DataFrame created at runtime
    */
  def returnSampleDFType2(sqlContext: SQLContext): DataFrame = {
    sqlContext
      .createDataFrame(
        Seq(
          ("0", "Agriculture"),
          ("1", "Mining"),
          ("2", "Construction"),
          ("3", "Manufacturing"),
          ("4", "Transportation"),
          ("5", "Wholesale Trade"),
          ("6", "Retail Trade"),
          ("7", "Finance")
        ))
      .toDF("code", "industry")
  }

  /** Sample DataFrame 3
    *
    * @param sqlContext SQLContext required for creating context
    * @return Sample DataFrame created at runtime
    */
  def returnSampleDFType3(sqlContext: SQLContext): DataFrame = {
    sqlContext
      .createDataFrame(
        Seq(
          ("0", "Agriculture", "check"),
          ("1", "Mining", "check"),
          ("2", "Construction", "check"),
          ("3", "Manufacturing", "check"),
          ("4", "Transportation", "check"),
          ("5", "Wholesale Trade", "check"),
          ("6", "Retail Trade", "check"),
          ("7", "Finance", "check"),
          ("8", "Services", "check"),
          ("9", "Public Administration", "check")
        ))
      .toDF("code", "industry", "check")
  }

  /** Sample DataFrame 4
    *
    * @param sqlContext SQLContext required for creating context
    * @return Sample DataFrame created at runtime
    */
  def returnSampleDFType4(sqlContext: SQLContext): DataFrame = {
    val short: Short = 12
    val double: Double = 12.1
    val long: Long = 12
    val float: Float = 12.20f
    val date = java.sql.Date.valueOf("2017-03-21")
    val timestamp = java.sql.Timestamp.valueOf("2017-03-21 12:12:12")
    val boolean = true
    sqlContext
      .createDataFrame(
        Seq(
          (1, "Mining", short, double, long, date, timestamp, boolean, float)
        ))
      .toDF("col0", "col1", "col2", "col3", "col4", "col5", "col6", "col7", "col8")
  }

  /** Sample DataFrame 5
    *
    * @param sqlContext SQLContext required for creating context
    * @return Sample DataFrame created at runtime
    */
  def returnSampleDFType5(sqlContext: SQLContext): DataFrame = {
    val timestamp = java.sql.Timestamp.valueOf("2017-03-21 12:12:12")
    sqlContext
      .createDataFrame(
        Seq(
          ("0", timestamp)
        ))
      .toDF("code", "industry")
  }

  def returnSampleDFType6(sqlContext: SQLContext): DataFrame = {
    sqlContext
      .createDataFrame(
        Seq(
          ("0", "Agriculture", 3),
          ("1", "Mining", 5),
          ("2", "Construction", 4),
          ("3", "Manufacturing", 1),
          ("4", "Transportation", 2)
        ))
      .toDF("code", "industry", "rank")
  }

}

