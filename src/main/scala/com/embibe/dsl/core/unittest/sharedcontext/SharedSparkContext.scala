package com.embibe.dsl.core.unittest.sharedcontext

import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext
import org.scalatest.{BeforeAndAfterAll, Suite}

/**
  * SharedSparkContext
  *
  * Created by Mukul Dev on 8th October 2018.
  */
trait SharedSparkContext extends BeforeAndAfterAll {
  this: Suite =>

  private var _sc: SparkContext = _
  private var _sqlContext: SQLContext = _

  override def beforeAll(): Unit = {
    super.beforeAll()
    val conf = new SparkConf()
      .setMaster("local[*]")
      .setAppName(this.getClass.getSimpleName)
    _sc = new SparkContext(conf)
    _sqlContext = new SQLContext(_sc)
    Logger.getLogger("org").setLevel(Level.ERROR)
  }

  override def afterAll(): Unit = {
    if (_sc != null) {
      _sqlContext = null
      _sc.stop()
      _sc = null
    }
    super.afterAll()
  }

  def sc: SparkContext = _sc

  def sqlContext: SQLContext = _sqlContext
}
