package com.embibe.dsl.core.unittest.dataframe

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.types.{StructField, StructType}

/** Perform Operations on DataFrame
  *
  * Created by Mukul Dev on 8th October 2018.
  */
object DataFrameUtility {

  /** Compare two Data Frame whether they are same
    *
    * Checks whether the data frame have same columns.
    * If column name are same then checks whether the data are same
    *
    * @param xDF First DataFrame to compare
    * @param yDF Second DataFrame to compare
    */
  def assertDataFrame(xDF: DataFrame, yDF: DataFrame): Unit = {

    // comparing columns of both the dataframes.

    val xColumnNameList = xDF.columns.toList
    val yColumnNameList = yDF.columns.toList
    assert(xColumnNameList.forall(x => yColumnNameList.contains(x)), "Column Name do not match - xDF : "
      + xColumnNameList.mkString(",") + " - yDF : " + yColumnNameList.mkString(","))
    assert(yColumnNameList.forall(y => xColumnNameList.contains(y)), "Column Name do not match - yDF : "
      + yColumnNameList.mkString(",") + " - xDF : " + xColumnNameList.mkString(","))

    val selectYDF = yDF.select(xColumnNameList.head, xColumnNameList.tail: _*)
    val selectXDF = xDF.select(yColumnNameList.head, yColumnNameList.tail: _*)

    // comparing schema of both the dataframes.

    val nullableSetXDF = setNullableForAllColumns(selectXDF)
    val nullableSetYDF = setNullableForAllColumns(selectYDF)

    val xDFSchema = nullableSetXDF.schema
    val yDFSchema = nullableSetYDF.schema

    assert(xDFSchema.diff(yDFSchema).lengthCompare(0) == 0, "xDF schema does not match yDF schema")

    // comparing contents of both the dataframes.

    if (xDF.except(selectYDF).count() != 0) {
      xDF.show()
      selectYDF.show()
      throw new AssertionError("assertion failed: xDF contents does not match yDF")
    }
    else if (yDF.except(selectXDF).count() != 0) {
      yDF.show()
      selectXDF.show()
      throw new AssertionError("assertion failed: yDF contents does not match xDF")
    }
  }

  /** Set nullable column for all the structfield of a dataframe to true.
    *
    * @param df: DataFrame
    * @param nullable: Boolean
    * @return
    */
  private def setNullableForAllColumns(df : DataFrame, nullable : Boolean = true) : DataFrame = {
    df.sqlContext.createDataFrame(df.rdd, StructType(df.schema.map(_.copy(nullable = nullable))))
  }

  /** Union 2 DF and if the schema of both the DataFrame
    *  are same return the unioned DF else throw exception.
    *
    * @param xDF: DataFrame - First DF to union
    * @param yDF: DataFrame - Second DF to union
    * @return
    */
  def unionDataFrame(xDF: DataFrame, yDF: DataFrame): DataFrame = {
    val xDFSchema = xDF.schema
    val yDFSchema = yDF.schema

    val combinedSchema = (xDFSchema ++ yDFSchema).distinct
    val combinedColumns = (xDF.columns ++ yDF.columns).distinct

    if (combinedSchema.length != combinedColumns.length) {
      val combinedSchemaColumns = combinedSchema.map(_.name).toList
      val columnWithTypeDifference = combinedSchemaColumns.diff(combinedColumns.toList)

      throw new IllegalStateException(s"${getClass.getName} - DataFrame cannot be unioned -" +
        s" conflicting dataypes of column - ${columnWithTypeDifference.mkString(",")}")
    } else {
      val xDFDeltaSchema = combinedSchema.toSet.diff(xDFSchema.toSet)
      val yDFDeltaSchema = combinedSchema.toSet.diff(yDFSchema.toSet)

      val xDFComplete = addDeltaColumns(xDFDeltaSchema, combinedColumns, xDF)
      val yDFComplete = addDeltaColumns(yDFDeltaSchema, combinedColumns, yDF)

      xDFComplete.union(yDFComplete)
    }
  }

  /** Add the difference columns to Dataframe
    *
    * @param deltaSchema: Seq[StructField]
    * @param combinedColumns: Array[String]
    * @param initialDF: DataFrame
    * @return
    */
  private def addDeltaColumns(deltaSchema: Set[StructField], combinedColumns: Array[String],
                              initialDF: DataFrame): DataFrame = {
    if (deltaSchema.nonEmpty) {
      deltaSchema.foldLeft(initialDF)((df, column) => {
        df.withColumn(column.name, lit(null).cast(column.dataType).alias(column.name))
      }).select(combinedColumns.head, combinedColumns.tail: _*)
    } else {
      initialDF
    }
  }
}
