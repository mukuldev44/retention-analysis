package com.embibe.dsl.core.etl.dataframe.extractload.file

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.col

/** Avro File Read and Write
  *
  * Created by Mukul Dev on 9th October 2018
  */
class AvroExtractLoad(_config: Config) extends FileExtractLoad {
  override val config: Config = _config

  /** Write DataFrame as per config
    *
    * @param inputDF Data Frame which is to be written
    */
  override def writeDataFrame(inputDF: DataFrame): Unit = {
    val optionConfig = config.getConfig("option")
    val columnRenamedDF = renameColumnDataFrame(inputDF)

    val writeDF = if (optionConfig.hasPath("columnNames")) {
      val writeColumns = optionConfig.getStringList("columnNames").asScala.toList
      columnRenamedDF.select(writeColumns.map(col): _*)
    }
    else {
      columnRenamedDF
    }
    // Note: always use `df.rdd.isEmpty` instead of `df.head.isEmpty` -> `df.head` is an action event
    if (writeDF.rdd.isEmpty ) {
    //scalastyle:off
      println(s"${getClass.getName} - Cannot write as the dataframe is empty")
    // scalastyle:on
    }
    else {

      val filePath = optionConfig.getString("path")

      val partitions = optionConfig.getStringList("partitionBy").asScala.toList

      val saveModeString = if (optionConfig.hasPath("saveMode")) {
        optionConfig.getString("saveMode").toLowerCase
      } else {
        "errorifexists"
      }
      val saveMode = getSaveMode(saveModeString)

      val coalescedDf = if (optionConfig.hasPath("coalesce")) {
        writeDF.coalesce(optionConfig.getInt("coalesce"))
      } else {
        writeDF
      }

      coalescedDf
          .write
          .mode(saveMode)
          .format("com.databricks.spark.avro")
          .partitionBy(partitions: _*)
          .save(filePath)
    }
  }

  /** Read Avro as DataFrame as per config
    *
    * @param sqlContext Spark SQLContext is required to create DataFrame
    * @return Read DataFrame as per configuration
    */
  override def readDataFrame(sqlContext: SQLContext): DataFrame = {
    val optionConfig = config.getConfig("option")
    val resourceFile = if (optionConfig.hasPath("resource")) {
      optionConfig.getBoolean("resource")
    } else {
      false
    }

    val filePath = optionConfig.getString("path")
    val updatedFilePath = if (resourceFile) {
      getClass.getResource(filePath).getFile
    } else {
      filePath
    }

    val readDF = transformDataFrame(sqlContext.read.format("com.databricks.spark.avro").load(updatedFilePath))
    if (optionConfig.hasPath("columnNames") && !optionConfig.getStringList("columnNames").isEmpty) {
      val columnNameList = optionConfig.getStringList("columnNames").asScala.toList
      readDF.select(columnNameList.head, columnNameList.tail: _*)
    }
    else {
      readDF
    }
  }
}
