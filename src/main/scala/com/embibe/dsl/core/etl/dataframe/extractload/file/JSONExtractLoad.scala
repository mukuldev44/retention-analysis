package com.embibe.dsl.core.etl.dataframe.extractload.file

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SQLContext}

/** JSON Reader and Writer Implementation
  *
  * Created by Mukul Dev on 9th October 2018.
  */
class JSONExtractLoad(_config: Config) extends FileExtractLoad {
  override val config: Config = _config

  /** Write DataFrame as per config
    *
    * @param inputDF Input Data Frame which is to be written
    */
  override def writeDataFrame(inputDF: DataFrame): Unit = {
    val optionConfig = config.getConfig("option")
    val columnRenamedDF = renameColumnDataFrame(inputDF)

    val writeDF = if (optionConfig.hasPath("columnNames")) {
      val writeColumns = optionConfig.getStringList("columnNames").asScala.toList
      columnRenamedDF.select(writeColumns.map(col): _*)
    }
    else {
      columnRenamedDF
    }

    val filePath = optionConfig.getString("path")

    val partitions = if (optionConfig.hasPath("partitionBy")) {
      optionConfig.getStringList("partitionBy").asScala.toList
    } else {
      List.empty
    }

    val saveModeString = if (optionConfig.hasPath("saveMode")) {
      optionConfig.getString("saveMode").toLowerCase
    } else {
      "errorifexists"
    }
    val saveMode = getSaveMode(saveModeString)

    val coalesceWriteDF = if (optionConfig.hasPath("coalesce")) {
      writeDF.coalesce(optionConfig.getInt("coalesce"))
    } else {
      writeDF
    }

    coalesceWriteDF
      .write
      .mode(saveMode)
      .partitionBy(partitions: _*)
      .json(filePath)
  }

  /** Read Parquet as DataFrame as per config
    *
    * @param sqlContext Spark SQLContext is required to create DataFrame
    * @return Read DataFrame as per configuration
    */
  override def readDataFrame(sqlContext: SQLContext): DataFrame = {
    val optionConfig = config.getConfig("option")
    val filePath = optionConfig.getString("path")
    // Specifically for test cases
    val resourceFile = if (optionConfig.hasPath("resource")) {
      optionConfig.getBoolean("resource")
    } else {
      false
    }
    val updatedFilePath = if (resourceFile) {
      getClass.getResource(filePath).getFile
    } else {
      filePath
    }
    // TODO: Test schema - if null in else works..
    val schemaString: String = if(optionConfig.hasPath("schema")){
      optionConfig.getString("schema")
    } else {
      null
    }
    val readDF = transformDataFrame{
      sqlContext
        .read
        .schema(schemaString)
        .json(updatedFilePath)
    }
    // Selecting Column Names
    if (optionConfig.hasPath("columnNames") && !optionConfig.getStringList("columnNames").isEmpty) {
      val columnNameList = optionConfig.getStringList("columnNames").asScala.toList
      readDF.select(columnNameList.head, columnNameList.tail: _*)
    }
    else {
      readDF
    }
  }
}
