package com.embibe.dsl.core.etl.dataframe

import com.embibe.dsl.core.etl.dataframe.extractload.datastore.DataStoreExtractLoad
import com.embibe.dsl.core.etl.dataframe.extractload.file.FileExtractLoad
import com.embibe.dsl.core.etl.dataframe.transform._

import scala.collection.JavaConverters._
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.{DataFrame, SQLContext}

/** Base Trait for Input Output DataFrameReadWriteFormat
  *
  * Created by Mukul Dev on 9th October 2018
  */
trait DataFrameExtractLoad {

  val config: Config

  /** Read DataFrame as per config
    *
    * @param sqlContext Spark SQLContext is required to create DataFrame
    * @return Read DataFrame as per configuration
    */
  def readDataFrame(sqlContext: SQLContext): DataFrame

  /** Write DataFrame as per config
    *
    * @param df Data Frame which is to be written
    */
  def writeDataFrame(df: DataFrame): Unit

  /**
    *
    * @param df Data Frame Which Needed To Format
    * @return Data Frame
    */
  def transformDataFrame(df: DataFrame): DataFrame = {
    if(config.hasPath("transform")) {
      val literalConfig = if (config.hasPath("literal")) {
        config.getConfig("literal")
      } else {
        ConfigFactory.empty()
      }
      val castConfig = if (config.hasPath("cast")) {
        config.getConfig("cast")
      } else {
        ConfigFactory.empty()
      }
      val forkConfig = if (config.hasPath("fork")) {
        config.getConfig("fork")
      } else {
        ConfigFactory.empty()
      }
      // TODO Predicate Push Down Filters
      val filterConfigList = if (config.hasPath("filter_list")) {
        config.getConfigList("filter_list").asScala.toList
      } else {
        List.empty
      }
      val literalDF = AddLiteralColumn.addLiteralColumnDataFrame(df, literalConfig)
      val castColumnDF = CastColumn.castColumnDataFrame(literalDF, castConfig)
      val forkDF = ForkColumn.forkColumnDataFrame(castColumnDF, forkConfig)
      val filterDF = FilterDataFrame.getFilterDataFrame(forkDF, filterConfigList)
      filterDF
    } else {
      df
    }
  }

  /** Rename columns in DataFrame if 'columnRename' key is present in the config
    *
    * @param dataFrame DataFrame in which columns are to be renamed
    * @return DataFrame with renamed columns
    */
  def renameColumnDataFrame(dataFrame: DataFrame): DataFrame = {
    if (config.hasPath("rename")) {
      val columnRenameConfig = config.getConfig("rename")
      ColumnRename.columnRenameDataFrame(dataFrame, columnRenameConfig)
    } else {
      dataFrame
    }
  }

  /** Adds column names in the dataframe if 'headersList' key is present in the config
    *
    * @param dataFrame Dataframe in which column names are to be added
    * @return Dataframe with added header columns
    */
  def addHeader(dataFrame: DataFrame): DataFrame = {
    if (config.hasPath("headersList")) {
      val columnNameList: List[String] = config.getStringList("headersList").asScala.toList
      val dataFrameColumnNameList = dataFrame.columns.toList
      require(columnNameList.length == dataFrameColumnNameList.length,
        "Header column count can not be different from input file column count")
      val columnNameTupleList = dataFrameColumnNameList.zip(columnNameList)
      val columnList = columnNameTupleList.map {
        case (oldName, newName) => dataFrame(oldName).as(newName)
      }
      dataFrame.select(columnList: _*)
    } else {
      dataFrame
    }
  }

}

/** Factory for DataFrameReadWriteFormat Class
  *
  */
object DataFrameExtractLoad {
  def apply(config: Config): DataFrameExtractLoad = {
    val formatType = config.getString("type").toLowerCase
    formatType match {
      case s if s.startsWith("file") => FileExtractLoad(config)
      case s if s.startsWith("datastore") => DataStoreExtractLoad(config)
      case _ =>
        throw new IllegalArgumentException(
          "DataFrameReadWriteFormat does not support specified format type : " + formatType)
    }
  }
}
