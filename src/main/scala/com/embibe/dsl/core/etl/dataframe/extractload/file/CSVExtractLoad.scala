package com.embibe.dsl.core.etl.dataframe.extractload.file

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.col

/** CSV Reader and Writer Implementation
  *
  * Created by Mukul Dev on 9th October 2018.
  */
class CSVExtractLoad(_config: Config) extends FileExtractLoad {
  override val config: Config = _config

  /** Write DataFrame as per config
    *
    * @param inputDF Input Data Frame which is to be written
    */
  override def writeDataFrame(inputDF: DataFrame): Unit = {
    val configOption = config.getConfig("option")

    val columnRenamedDF = renameColumnDataFrame(inputDF)

    val writeDF = if (configOption.hasPath("columnNames")) {
      val writeColumns = configOption.getStringList("columnNames").asScala.toList
      columnRenamedDF.select(writeColumns.map(col): _*)
    }
    else {
      columnRenamedDF
    }

    val filePath = configOption.getString("path")

    val fileDelimiter = if (configOption.hasPath("delimiter")) {
      configOption.getString("delimiter")
    } else {
      ","
    }
    val header = if (configOption.hasPath("header")) {
      configOption.getBoolean("header").toString
    } else {
      "false"
    }
    val quote = if (configOption.hasPath("quote")) {
      configOption.getString("quote")
    } else {
      "\""
    }
    val escape = if (configOption.hasPath("escape")) {
      configOption.getString("escape")
    } else {
      "\\"
    }
    val nullValue = if (configOption.hasPath("nullValue")) {
      configOption.getString("nullValue")
    } else {
      "null"
    }
    val quoteMode = if (configOption.hasPath("quoteMode")) {
      configOption.getString("quoteMode")
    } else {
      "MINIMAL"
    }
    val dateFormat = if (configOption.hasPath("dateFormat")) {
      configOption.getString("dateFormat")
    } else {
      "yyyy-MM-dd HH:mm:ss.S"
    }
    val csvOptionMap = Map("header" -> header,
      "delimiter" -> fileDelimiter,
      "quote" -> quote,
      "escape" -> escape,
      "nullValue" -> nullValue,
      "quoteMode" -> quoteMode,
      "dateFormat" -> dateFormat)

    val saveModeString = if (configOption.hasPath("saveMode")) {
      configOption.getString("saveMode").toLowerCase
    } else {
      "errorifexists"
    }
    val saveMode = getSaveMode(saveModeString)
    val coalesceWriteDF = if (configOption.hasPath("coalesce")) {
      writeDF.coalesce(configOption.getInt("coalesce"))
    } else {
      writeDF
    }

    coalesceWriteDF
      .write
      .mode(saveMode)
      .options(csvOptionMap)
      .csv(filePath)
  }

  /** Read CSV as DataFrame as per config
    *
    * @param sqlContext Spark SQLContext is required to create DataFrame
    * @return Read DataFrame as per configuration
    */
  override def readDataFrame(sqlContext: SQLContext): DataFrame = {
    val optionConfig = config.getConfig("option")
    val filePath = optionConfig.getString("path")
    // Specifically for test cases
    val resourceFile = if (optionConfig.hasPath("resource")) {
      optionConfig.getBoolean("resource")
    } else {
      false
    }
    val updatedFilePath = if (resourceFile) {
      getClass.getResource(filePath).getFile
    } else {
      filePath
    }

    val fileDelimiter = if (optionConfig.hasPath("delimiter")) {
      optionConfig.getString("delimiter")
    } else {
      ","
    }
    val inferSchema = if (optionConfig.hasPath("inferSchema")) {
      optionConfig.getString("inferSchema")
    } else {
      "false"
    }
    val header = if (optionConfig.hasPath("header")) {
      optionConfig.getString("header")
    } else {
      "false"
    }
    val quote = if (optionConfig.hasPath("quote")) {
      optionConfig.getString("quote")
    } else {
      "\""
    }
    val escape = if (optionConfig.hasPath("escape")) {
      optionConfig.getString("escape")
    } else {
      "\\"
    }
    val nullValue = if (optionConfig.hasPath("nullValue")) {
      optionConfig.getString("nullValue")
    } else {
      "null"
    }
    val mode = if (optionConfig.hasPath("mode")) {
      optionConfig.getString("mode")
    } else {
      "PERMISSIVE"
    }
    val charset = if (optionConfig.hasPath("charset")) {
      optionConfig.getString("charset")
    } else {
      "UTF-8"
    }
    val comment = if (optionConfig.hasPath("comment")) {
      optionConfig.getString("comment")
    } else {
      "#"
    }
    val parserLib = if (optionConfig.hasPath("parserLib")) {
      optionConfig.getString("parserLib")
    } else {
      "commons"
    }
    val dateFormat = if (optionConfig.hasPath("dateFormat")) {
      optionConfig.getString("dateFormat")
    } else {
      "null"
    }

    val csvOptions = Map(
      "header" -> header,
      "delimiter" -> fileDelimiter,
      "inferSchema" -> inferSchema,
      "quote" -> quote,
      "escape" -> escape,
      "nullValue" -> nullValue,
      "mode" -> mode,
      "charset" -> charset,
      "comment" -> comment,
      "parserLib" -> parserLib,
      "dateFormat" -> dateFormat
    )
    val readDF = transformDataFrame{
      sqlContext
        .read
        .options(csvOptions)
        .csv(updatedFilePath)
    }
    // Selecting Column Names
    if (optionConfig.hasPath("columnNames") && !optionConfig.getStringList("columnNames").isEmpty) {
      val columnNameList = optionConfig.getStringList("columnNames").asScala.toList
      addHeader(readDF.select(columnNameList.head, columnNameList.tail: _*))
    }
    else {
      addHeader(readDF)
    }
  }
}
