package com.embibe.dsl.core.etl.dataframe.transform

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.lit


/**
  */
object AddLiteralColumn {

  /**
    * Adds literal columns to the provided dataframe.
    * If a column already exists then its values will be overwritten.
    *
    * @param inputDF       DataFrame to add literal columns
    * @param literalConfig Json of type
    *                      `{ "column1": "someLit",
    *                      "column2": "2"
    *                      }
    *                      `
    */

  def addLiteralColumnDataFrame(inputDF: DataFrame, literalConfig: Config): DataFrame = {
    if (literalConfig.isEmpty) {
      inputDF
    } else {
      val columnNames = literalConfig.root().entrySet().asScala.map(_.getKey)
      val outputDF = columnNames.foldLeft(inputDF) { (df, col) =>
        df.withColumn(col, lit(literalConfig.getString(col)))
      }
      outputDF
    }
  }

}
