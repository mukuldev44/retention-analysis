package com.embibe.dsl.core.etl.dataframe.extractload.file

import com.embibe.dsl.core.etl.dataframe.DataFrameExtractLoad
import com.typesafe.config.Config
import org.apache.spark.sql.SaveMode

/** File Base Trait
  *
  * Created by Mukul Dev on 2/2/17.
  */
trait FileExtractLoad extends DataFrameExtractLoad {

  /** Save Mode Object Factory
    *
    * @param saveModeString Save Mode String Format
    * @return Save Mode Object
    */
  protected def getSaveMode(saveModeString: String): SaveMode = {
    saveModeString match {
      case "overwrite" => SaveMode.Overwrite
      case "append" => SaveMode.Append
      case "ignore" => SaveMode.Ignore
      case "errorifexists" => SaveMode.ErrorIfExists
      case _ =>
        throw new IllegalArgumentException(
          "File does not support specified SaveMode option : " + saveModeString)
    }
  }
}

/** File Factory - Returns Specified File Type Object
  */
object FileExtractLoad {

  /**
    * @param config Input Config
    */
  def apply(config: Config): FileExtractLoad = {
    val fileType = config.getString("type").toLowerCase
    fileType match {
      case s if s.endsWith("csv") => new CSVExtractLoad(config)
      case s if s.endsWith("avro") => new AvroExtractLoad(config)
      case s if s.endsWith("parquet") => new ParquetExtractLoad(config)
      case s if s.endsWith("json") => new JSONExtractLoad(config)
      case _ =>
        throw new IllegalArgumentException(
          "File does not support specified type : " + fileType)
    }
  }
}
