package com.embibe.dsl.core.etl.dataframe.extractload.datastore

import com.embibe.dsl.core.etl.dataframe.DataFrameExtractLoad
import com.typesafe.config.Config


/** DataBaseDataFrameReadWriteFormat Trait
  *
  */
trait DataStoreExtractLoad extends DataFrameExtractLoad

/**
  * Companion Factory of database
  */
object DataStoreExtractLoad {
  def apply(config: Config): DataStoreExtractLoad = {
    val outputType = config.getString("type").toLowerCase
    outputType match {
      case s if s.endsWith("hbase") => new HBaseExtractLoad(config)
      case m if m.endsWith("elasticsearch") => new ElasticSearchExtractLoad(config)
      case _ => throw new IllegalArgumentException("DataBaseDataFrameReadWriteFormat" +
        " currently do not support specified type of datastore " + outputType)
    }
  }
}
