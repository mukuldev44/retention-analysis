package com.embibe.dsl.core.etl.dataframe.extractload.datastore

import com.embibe.dsl.core.etl.dataframe.DataFrameExtractLoad
import com.typesafe.config.{Config, ConfigRenderOptions}
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Column, DataFrame, SQLContext}
import org.elasticsearch.spark.sql._

import scala.collection.JavaConverters._
import scala.util.parsing.json.JSONObject

/** Elastic Search Read and Write
  *
  */
class ElasticSearchExtractLoad(_config: Config) extends DataStoreExtractLoad {
  override val config: Config = _config

  override def readDataFrame(sqlContext: SQLContext): DataFrame = ???

  override def writeDataFrame(df: DataFrame): Unit = ???
}


/** ElasticSearchDataFrameReadWriteFormat Factory Implementation
  */
object ElasticSearchExtractLoad {
  /** Factory Method for Elastic Search
    *
    * @param config Input Config
    * @return Elastic Search Object
    */
  def apply(config: Config): ElasticSearchExtractLoad = {
    new ElasticSearchExtractLoad(config)
  }
}
