package com.embibe.dsl.core.etl.dataframe.extractload.datastore

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SQLContext}

/** HBase Read and Write
  *
  */
class HBaseExtractLoad(_config: Config) extends DataStoreExtractLoad {
  override val config: Config = _config

  override def readDataFrame(sqlContext: SQLContext): DataFrame = ???

  override def writeDataFrame(df: DataFrame): Unit = ???
}