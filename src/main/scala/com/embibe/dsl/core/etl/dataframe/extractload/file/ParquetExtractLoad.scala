package com.embibe.dsl.core.etl.dataframe.extractload.file

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.col

/** Parquet Reader and Writer Implementation
  *
  */
class ParquetExtractLoad(_config: Config) extends FileExtractLoad {
  override val config: Config = _config

  /** Write DataFrame as per config
    *
    * @param inputDF Input Data Frame which is to be written
    */
  override def writeDataFrame(inputDF: DataFrame): Unit = {
    val optionConfig = config.getConfig("option")
    val columnRenamedDF = renameColumnDataFrame(inputDF)

    val writeDF = if (optionConfig.hasPath("columnNames")) {
      val writeColumns = optionConfig.getStringList("columnNames").asScala.toList
      columnRenamedDF.select(writeColumns.map(col): _*)
    }
    else {
      columnRenamedDF
    }

    val filePath = optionConfig.getString("path")

    val partitions = if (optionConfig.hasPath("partitionBy")) {
      optionConfig.getStringList("partitionBy").asScala.toList
    } else {
      List.empty
    }

    val saveModeString = if (optionConfig.hasPath("saveMode")) {
      optionConfig.getString("saveMode").toLowerCase
    } else {
      "errorifexists"
    }
    val saveMode = getSaveMode(saveModeString)

    val coalesceWriteDF = if (optionConfig.hasPath("coalesce")) {
      writeDF.coalesce(optionConfig.getInt("coalesce"))
    } else {
      writeDF
    }

    coalesceWriteDF
      .write
      .mode(saveMode)
      .partitionBy(partitions: _*)
      .parquet(filePath)
  }

  /** Read Parquet as DataFrame as per config
    *
    * @param sqlContext Spark SQLContext is required to create DataFrame
    * @return Read DataFrame as per configuration
    */
  override def readDataFrame(sqlContext: SQLContext): DataFrame = {
    val optionConfig = config.getConfig("option")
    val filePath = optionConfig.getString("path")
    val resourceFile = if (optionConfig.hasPath("resource")) {
      optionConfig.getBoolean("resource")
    } else {
      false
    }

    val updatedFilePath = if (resourceFile) {
      getClass.getResource(filePath).getFile
    } else {
      filePath
    }
    val readDF = transformDataFrame(sqlContext.read.parquet(updatedFilePath))
    if (optionConfig.hasPath("columnNames") && !optionConfig.getStringList("columnNames").isEmpty) {
      val columnNameList = optionConfig.getStringList("columnNames").asScala.toList
      readDF.select(columnNameList.head, columnNameList.tail: _*)
    }
    else {
      readDF
    }
  }
}
