package com.embibe.dsl.core.etl.dataframe.transform

import com.typesafe.config.Config
import org.apache.spark.sql.DataFrame

/**
  */
object ColumnRename {

  /**
    * Rename dataframe column name
    *
    * @param df                 Input dataframe
    * @param columnRenameConfig Column Rename Config
    * @return OutputDataframe
    */
  def columnRenameDataFrame(df: DataFrame, columnRenameConfig: Config): DataFrame = {
    val columnProjections = df.schema.fields.map { col =>
      if (columnRenameConfig.hasPath(col.name)) {
        val columnRename = columnRenameConfig.getString(col.name)
        df.col(col.name).as(columnRename)
      } else {
        df.col(col.name)
      }
    }
    df.select(columnProjections: _*)
  }

  /**
    *
    * @param ipDF
    * @param commonName
    * @return DataFarme
    */
  def appendCommonNameToColumnNameDF(ipDF: DataFrame,
                                     commonName: String): DataFrame = {
    appendCommonNameToColumnNameDF(ipDF, commonName, ":")
  }

  /**
    * Appends a common name to each column in DataFrame
    *
    * @param ipDF       DataFrame whose columns need to be appended with a common name
    * @param commonName Common Name which will be appended to each column in DataFrame
    * @param separator  Separator Appended Between Common Name
    * @return DataFrame with the appended Column Name
    */
  def appendCommonNameToColumnNameDF(ipDF: DataFrame,
                                     commonName: String,
                                     separator: String): DataFrame = {
    val columnNameList = ipDF.columns
    val ipDFColumnList = columnNameList.map { columnName =>
      ipDF.col(columnName).as(commonName + separator + columnName)
    }
    ipDF.select(ipDFColumnList: _*)
  }

}
