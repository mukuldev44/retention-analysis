package com.embibe.dsl.core.etl.dataframe

import com.typesafe.config.Config
import org.apache.spark.sql.{Column, DataFrame, SQLContext}
import org.apache.spark.sql.functions._

/** Multi Format Read and Write
  */
class MultiDataFrameExtractLoad(configList: List[Config]) {

  /** Read DataFrame as per config
    *
    * @param sqlContext Spark SQLContext is required to create DataFrame
    * @return Read DataFrame as per configuration
    *
    */

  def readDataFrame(sqlContext: SQLContext): DataFrame = {
    val dfList = configList.map { config =>
      val format = DataFrameExtractLoad(config)
      format.readDataFrame(sqlContext)
    }
    val startDF = sqlContext.emptyDataFrame
    val finalDF = dfList.foldLeft(startDF) { case (firstDF, secondDF) =>
      val firstDFColumnSet = firstDF.columns.toSet
      val secondDFColumnSet = secondDF.columns.toSet
      val totalColumns = firstDFColumnSet ++ secondDFColumnSet
      firstDF.select(addNullColumns(firstDFColumnSet, totalColumns): _*)
        .unionAll(secondDF.select(addNullColumns(secondDFColumnSet, totalColumns): _*))
    }
    finalDF
  }

  /** Add extra null columns to dataframe
    *
    * @param dfCols    DataFrame Columns
    * @param totalCols Total Columns
    * @return Final Columns
    */

  def addNullColumns(dfCols: Set[String], totalCols: Set[String]): List[Column] = {
    totalCols.toList.map(column => column match {
      case columnName if dfCols.contains(columnName) => col(columnName)
      case _ => lit(null).as(column)
    })
  }

  /** Write DataFrame as per config
    *
    * @param inputDF Input Data Frame which is to be written
    *
    */

  def writeDataFrame(inputDF: DataFrame): Unit = {
    configList.foreach { config =>
      val format = DataFrameExtractLoad(config)
      format.writeDataFrame(inputDF)
    }
  }
}

/** MultiFormatDataFrameReadWrite Factory Implementation
  */

object MultiDataFrameExtractLoad {
  /** Factory Method for MultiFormatDataFrameReadWrite
    *
    * @param configList Input Config List
    * @return MultiFormatDataFrameReadWrite Object
    */
  def apply(configList: List[Config]): MultiDataFrameExtractLoad = {
    new MultiDataFrameExtractLoad(configList: List[Config])
  }
}
