package com.embibe.dsl.core.etl.dataframe.transform

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.DataFrame


/**
  * Created by Rohit on 2/8/17.
  */
object ForkColumn {

  /**
    * Forks a column value to another column
    * If a column already exists then its values will be overwritten.
    *
    * @param inputDF    DataFrame to add fork columns
    * @param forkConfig Json of type
    *                   `{ "column1": "columnX",
    *                   "column2": "columnY"
    *                   }
    *                   `
    * @return Output DataFrame with forked columns
    */
  def forkColumnDataFrame(inputDF: DataFrame, forkConfig: Config): DataFrame = {
    if (forkConfig.isEmpty) {
      inputDF
    } else {
      val columnNames = forkConfig.root().entrySet().asScala.map(_.getKey)
      val outputDF = columnNames.foldLeft(inputDF) { (df, col) =>
        df.withColumn(col, df(forkConfig.getString(col)))
      }
      outputDF
    }
  }

}
