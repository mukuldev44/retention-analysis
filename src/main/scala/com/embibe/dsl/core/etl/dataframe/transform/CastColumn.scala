package com.embibe.dsl.core.etl.dataframe.transform

import scala.collection.JavaConverters._

import com.typesafe.config.Config
import org.apache.spark.sql.{Column, DataFrame}
import org.apache.spark.sql.functions._


/**
  */
object CastColumn {

  /**
    * Casts the columns of the DataFrame according to the casting config provided
    *
    * @param df         Data Frame Whose Columns Need To Cast
    * @param castConfig {"column1": ["String", "Date"], "column2": ["String", "Date"]}
    * @return DataFrame
    */

  def castColumnDataFrame(df: DataFrame, castConfig: Config): DataFrame = {
    if (castConfig.isEmpty) {
      df
    } else {
      val columnNames = castConfig.root().entrySet().asScala.map(_.getKey)
      val projections = df.dtypes.map { case (colName, colType) =>
        if (columnNames.contains(colName)) {
          val castingOrder = castConfig.getStringList(colName).asScala.toList
          val updatedCastingList = castingOrder.map(_.toLowerCase)
          castColumn(df(colName), colType, updatedCastingList).alias(colName)
        } else {
          df(colName)
        }
      }
      df.select(projections: _*)
    }
  }

  /**
    * Cast a column in the order of the casting list and return a Column
    *
    * @param column      (dataframe column)
    * @param castingList List["string", "double"]
    * @return Column
    */

  def castColumn(column: Column, colType: String, castingList: List[String]): Column = {
    castingList.foldLeft(column) { (col, casting) =>
      if (colType == "StringType" && casting == "boolean") {
        when(col === "true", true).otherwise(false)
      } else {
        col.cast(casting)
      }
    }
  }

}
