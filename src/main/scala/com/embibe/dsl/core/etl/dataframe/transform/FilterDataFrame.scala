package com.embibe.dsl.core.etl.dataframe.transform

import java.sql.Date

import com.typesafe.config.Config
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._

/** Filter Data Frame
  *
  *
  * */
object FilterDataFrame {

  /** Get Filter DataFrame
    *
    * @param inputDF          Input Data Frame
    * @param filterConfigList List Of Filter Config
    * @return Filter Data Frame
    */
  def getFilterDataFrame(inputDF: DataFrame, filterConfigList: List[Config]): DataFrame = {
    if (filterConfigList.isEmpty) {
      inputDF
    } else {
      val filterQuery = filterConfigList.map { config =>
        val operator = config.getString("operator")
        val leftColumn = config.getString("left_column")
        val columnStructField = inputDF.schema.find(structField => structField.name == leftColumn)
          .getOrElse(
            throw new IllegalArgumentException(s"${getClass.getSimpleName} $leftColumn" +
              s" Not present in Input Data Frame"))

        val castValue = if (config.hasPath("value")) {
          val value = config.getString("value")
          castValueDataType(columnStructField, value)
        }
        else {
          val rightColumn = config.getString("right_column")
          inputDF(rightColumn)
        }

        operator match {
          case ">" => inputDF(leftColumn).gt(castValue)
          case ">=" => inputDF(leftColumn).geq(castValue)
          case "<" => inputDF(leftColumn).lt(castValue)
          case "<=" => inputDF(leftColumn).leq(castValue)
          case "=" => inputDF(leftColumn) === castValue
          case "!=" => inputDF(leftColumn).notEqual(castValue)
          case "<=>" => inputDF(leftColumn) <=> castValue || inputDF(leftColumn).isNull
          case "!<=>" => !(inputDF(leftColumn) <=> castValue)
          case _ => throw new IllegalArgumentException(s"${getClass.getName} does not support specified " +
            s"operator $operator")
        }
      }.reduce(_ && _)
      inputDF.filter(filterQuery)
    }
  }

  /** Cast The Value Into Given Type
    *
    * @param columnStructField Column Struct Field
    * @param value    Value Which Need To Cast
    * @return
    */
  private def castValueDataType(columnStructField: StructField, value: String): Any = {
    val dataType = columnStructField.dataType
    try {
      dataType match {
        case StringType => String.valueOf(value)
        case DateType => Date.valueOf(value)
        case BooleanType => value.toBoolean
        case FloatType => value.toFloat
        case LongType => value.toLong
        case DoubleType => value.toDouble
        case IntegerType => value.toInt
      }
    } catch {
      case _: MatchError => throw new IllegalArgumentException(s"${getClass.getSimpleName} does not support" +
        s" specified dataType $dataType")
      case _ => throw new IllegalArgumentException(s"${getClass.getSimpleName} Type of ${columnStructField.name}" +
        s" and $value does not match")
    }
  }
}

