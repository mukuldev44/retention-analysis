package com.embibe.dsl.retentionanalysis.executor

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType
import org.elasticsearch.spark.sql._

object RetentionAnalysisOutputRefactor {

  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()

    import spark.implicits._

    // Root Logger Level
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.ERROR)

    val exType = "subject"
    val codeList = List("su117", "su14", "su21", "su22", "su56", "su60", "su64", "su65", "su72", "su77", "su78")
    val rowKeyList = List("pivotYear", "pivot", "otherYear", "other", "analysisType", "advancedFilter")

    codeList.foreach{ x =>
      val df = spark.read.parquet(s"s3a://dsl-datastore/data/retention-analysis/$exType/$x/*/*")

      val renamedDF = df
        .withColumnRenamed("pivotMonth", "pivot")
        .withColumnRenamed("otherMonth", "other")

      val solDF = renamedDF
        .withColumn("analysisType", lit("month"))
        .withColumn("pivotView", getMonthNameUDF(col("pivotYear"), col("pivot")))
        .withColumn("otherView", getMonthNameUDF(col("otherYear"), col("other")))
        .withColumn("advancedFilter", lit(s"$exType" + s"_code=$x"))

      val solDfWithRowKey = solDF.withColumn("sno",
        concat_ws(":", rowKeyList.map(key => when(col(key).isNull, "").otherwise(col(key).cast(StringType))): _*)
      )

      val solDS = solDfWithRowKey.as[OutputSchema_Refactor]

      solDS.saveToEs("embibe-dsl-retention-analysis_v1/doc",
        Map(
          "es.nodes" -> "10.140.10.7",
          "es.port" -> "9200",
          "es.mapping.id" -> "sno"
        )
      )
    }

  }

  // -- Metnods
  val monthMap = Map(1 -> "Jan", 2 -> "Feb", 3 -> "Mar", 4 -> "Apr", 5 -> "May", 6 -> "Jun", 7 -> "Jul", 8 -> "Aug", 9 -> "Sep", 10 -> "Oct", 11 -> "Nov", 12 -> "Dec")
  val getMonthNameUDF = udf((year: Int, month: Int) => monthMap(month) + "-" + year)
}

// -- Data Classes
case class OutputSchema(
                         pivotYear: Int,
                         pivotMonth: Int,
                         otherYear: Int,
                         otherMonth: Int,
                         pivotCount: Int,
                         retentionCount: Int,
                         retentionPercentage: Double,
                         executionTimestamp: Long
                       )

case class OutputSchema_Refactor(
                                  pivotYear: Int,
                                  pivot: Int,
                                  pivotView: String,
                                  otherYear: Int,
                                  other: Int,
                                  otherView: String,
                                  analysisType: String,
                                  advancedFilter: String,
                                  pivotCount: Int,
                                  retentionCount: Int,
                                  retentionPercentage: Double,
                                  executionTimestamp: Long,
                                  sno: String
                                )
