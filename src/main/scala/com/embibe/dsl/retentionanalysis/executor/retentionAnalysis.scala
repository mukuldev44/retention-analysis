package com.embibe.dsl.retentionanalysis.executor

import java.sql.Date

import com.typesafe.config.{Config, ConfigException, ConfigFactory}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._
import org.apache.spark.storage.StorageLevel

import scala.collection.JavaConverters._


/** Retention Analysis
  *
  * spark-shell --master yarn --driver-memory 2G --executor-memory 2G --num-executors 8 --executor-cores 4 --packages org.elasticsearch:elasticsearch-hadoop:6.3.1,com.typesafe:config:1.3.2 --queue dev_cs_agg --name dev_shell
  */
object retentionAnalysis {
  def main(args: Array[String]): Unit = {
    // SparkSession
    val spark = SparkSession.builder
      .master("yarn-client")
      .appName(getClass.getSimpleName)
      .enableHiveSupport()
      .getOrCreate()

    import spark.implicits._

    // Root Logger Level
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.ERROR)

//    val oldBasePath = "s3a://segmentio.logs/segment-logs/WVFrRt7TUs/"
//    val newBasePath = "s3a://segmentio.logs/segment-logs/MsJ8v1Jm7f/"
//    val startDate = "2018-07-01"
//    val endDate = "2018-10-20"
//    val epochList = getTimestampRange(startDate, endDate)
//    val oldVarPath = getVarInputPath(oldBasePath, epochList)
//    val newVarPath = getVarInputPath(newBasePath, epochList)
//    val inputVarPath = oldVarPath ++ newVarPath
////    val oldDF = spark.read.schema(inputSchema).json(oldVarPath: _*)
////    val newDF = spark.read.schema(inputSchema).json(newVarPath: _*)
//    val inputDF = spark.read.schema(inputSchema).json(inputVarPath: _*)

    val jobStartTimestamp = spark.sparkContext.startTime
    spark.time()
    val inputNonRearchBasePath = "s3a://dsl-datastore/data/ingested/clickstream/raw/non-rearch/"
    val inputRearchBasePath = "s3a://dsl-datastore/data/ingested/clickstream/raw/rearch/"
    val monthList = List("2018/04", "2018/05", "2018/06", "2018/07", "2018/08", "2018/09", "2018/10")
    val inputNonRearchPathList = monthList.map(x => inputNonRearchBasePath + x + "/*")
    val inputRearchPathList = monthList.map(x => inputRearchBasePath + x + "/*")
    val inputPathList = inputNonRearchPathList ++ inputRearchPathList
    val inputDF = spark.read.schema(inputSchema).json(inputPathList: _*)
    val inputDS = getInputDS(inputDF)

    // COV
    val basePath = "s3a://dsl-datastore/data/content/cov/prod/exam_wise_cov_data/20181014/"
    val fileList = List("aiims_cov_data.json", "bitsat_cov_data.json", "foundation-08_cov_data.json", "foundation-09_cov_data.json", "foundation-10_cov_data.json", "gujarat-cet_cov_data.json", "jee-advanced_cov_data.json", "jee-main_cov_data.json", "neet_cov_data.json")
    val varArgsList = fileList.map(x => basePath + x)
    val covDF = spark.read.schema(covSchema).json(varArgsList: _*)
    covDF.persist(StorageLevel.MEMORY_AND_DISK)

    // Broadcast Variables
    val goalCOVDF = getDistinctCovMap(covDF, "goal_code", "goal_name")
    val examCOVDF = getDistinctCovMap(covDF, "exam_code", "exam_name")
    val subjectCOVDF = getDistinctCovMap(covDF, "subject_code", "subject_name")
    val unitCOVDF = getDistinctCovMap(covDF, "unit_code", "unit_name")
    val chapterCOVDF = getDistinctCovMap(covDF, "chapter_code", "chapter_name")
    val contentCovList = getContentCovList(covDF.as[CovInputSchema])
    covDF.unpersist()

    val urlMappedDS = getUrlMappedDataSet(inputDS, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF, contentCovList)
    urlMappedDS.persist(StorageLevel.MEMORY_AND_DISK)

    List("su65", "su117", "su64", "su60", "su14", "su72", "su77", "su56", "su78", "su21", "su22").foreach{ x =>
      val urlAdvancedFilteredDS = urlMappedDS.filter(_.subject_code.equalsIgnoreCase(x))
      val distinctDS = getDistinctUserId(urlAdvancedFilteredDS)
      distinctDS.persist(StorageLevel.MEMORY_AND_DISK)
      val outputDS = sol(distinctDS, jobStartTimestamp)
      outputDS.write.parquet(s"s3a://dsl-datastore/data/retention-analysis/subject/$x/yaar=2018-month_range=04-10/$jobStartTimestamp")
      distinctDS.unpersist(true)
    }

    val gl8DF = spark.read.parquet("s3a://dsl-datastore/data/retention-analysis/goal/gl8/yaar=2018-month_range=04-10/1540384840271")

//    // Learning Concepts Maps
//    val goalDbPullDS = getCsv(spark, "s3a://dsl-datastore/data/content/cov/prod/learning_concept_maps/rearch_db_pull/rearch_goals.tsv")
//      .select("goal_code", "goal_name", "goal_slug")
//      .as[goalDbPullSchema]
//    val goalDbPullList = spark.sparkContext.broadcast(goalDbPullDS.collect().toList)
//
//    val examDbPullDS = getCsv(spark, "s3a://dsl-datastore/data/content/cov/prod/learning_concept_maps/rearch_db_pull/rearch_exams.tsv")
//      .select("exam_code", "exam_name", "exam_slug", "goal_code")
//      .as[examDbPullSchema]
//    val examDbPullList = spark.sparkContext.broadcast(examDbPullDS.collect().toList)
//
//    val subjectDbPullDS = getCsv(spark, "s3a://dsl-datastore/data/content/cov/prod/learning_concept_maps/rearch_db_pull/rearch_subjects.tsv")
//      .select("subject_code", "subject_name", "subject_slug", "subject_test_xpath_csv", "subject_practice_xpath_csv", "exam_code")
//      .as[subjectDbPullSchema]
//    val subjectDbPullList = spark.sparkContext.broadcast(subjectDbPullDS.collect().toList)
//
//    val unitDbPullDS = getCsv(spark, "s3a://dsl-datastore/data/content/cov/prod/learning_concept_maps/rearch_db_pull/rearch_units.tsv")
//      .select("unit_code", "unit_name", "unit_slug", "unit_test_xpath_csv", "unit_practice_xpath_csv", "subject_code")
//      .as[unitDbPullSchema]
//    val unitDbPullList = spark.sparkContext.broadcast(unitDbPullDS.collect().toList)
//
//    val chapterDbPullDS = getCsv(spark, "s3a://dsl-datastore/data/content/cov/prod/learning_concept_maps/rearch_db_pull/rearch_chapters.tsv")
//      .select("chapter_code", "chapter_name", "chapter_slug", "chapter_test_xpath_csv", "chapter_practice_xpath_csv", "unit_code")
//      .as[chapterDbPullSchema]
//    val chapterDbPullList = spark.sparkContext.broadcast(chapterDbPullDS.collect().toList)
//
//    val conceptDbPullDS = getCsv(spark, "s3a://dsl-datastore/data/content/cov/prod/learning_concept_maps/rearch_db_pull/rearch_concepts.tsv")
//      .select("concept_code", "concept_name", "concept_relevant_exams")
//      .as[conceptDbPullSchema]
//    val conceptDbPullList = spark.sparkContext.broadcast(conceptDbPullDS.collect().toList)
  }


  // TODO: do this part using aws java sdk
  // INFO: HACK
  def getTimestampRange(startDate: String, endDate: String) = {
    val startTimestamp = Date.valueOf(startDate).getTime
    val endTimestamp = Date.valueOf(endDate).getTime

    (startTimestamp to endTimestamp by 24*60*60*1000).map(_.toString).toList
  }

  def getVarInputPath(basePath: String, dirList: List[String]) = {
    dirList.map(dir => basePath + dir + "/*")
  }

  def getInputDS(df: DataFrame): Dataset[InputSchema] = {
    import df.sparkSession.implicits._

    val inputFlatDF = df
      .withColumn("user_id", trim(coalesce(col("userId"), col("properties.user_id"))))
      .withColumn("url", col("context.page.url"))
      .withColumn("path", col("context.page.path"))
      .drop("context")
      .drop("properties")
      .drop("userId")
      .filter(col("user_id").isNotNull && col("user_id") =!= "")

    val dateDF = inputFlatDF
      .withColumn("date", to_date(col("timestamp")))
      .withColumn("year", year(col("date")))
      .withColumn("month", month(col("date")))
      .withColumn("week", weekofyear(col("date")))

    val inputDS = dateDF.as[InputSchema]

    inputDS
  }

  def getCsv(spark: SparkSession, path: String): DataFrame = {
    spark.read.format("com.databricks.spark.csv")
      .option("header", "true")
      .option("delimiter", "\t")
      .option("inferSchema", "true")
      .load(path)
  }


  def getDistinctCovMap(df: DataFrame, codeColumn: String, nameColumn: String) = {
    val distinctDF = df.select(nameColumn, codeColumn)
      .distinct
      .filter(col(codeColumn).isNotNull && col(codeColumn) =!= "")
      .withColumnRenamed(codeColumn, "codeColumn")
      .withColumnRenamed(nameColumn, "nameColumn")

    val distinctMapArray = distinctDF
      .collect()
      .map(r => Map(distinctDF.columns.zip(r.toSeq.map(_.toString)): _*))
    val distinctMap = Map(distinctMapArray.map(p => (p("nameColumn").trim.toLowerCase, p)):_*)

    df.sparkSession.sparkContext.broadcast(distinctMap)
  }

  def getContentCovList(df: Dataset[CovInputSchema]) = {
    val distinctMapList = df
      .collect()
      .toList

    df.sparkSession.sparkContext.broadcast(distinctMapList)
  }

  def getUrlMappedDataSet(
              ds: Dataset[InputSchema],
              goalCOVDF: Broadcast[Map[String, Map[String, String]]],
              examCOVDF: Broadcast[Map[String, Map[String, String]]],
              subjectCOVDF: Broadcast[Map[String, Map[String, String]]],
              unitCOVDF: Broadcast[Map[String, Map[String, String]]],
              chapterCOVDF: Broadcast[Map[String, Map[String, String]]],
              contentCovList: Broadcast[scala.List[CovInputSchema]]
            ) = {
    import ds.sparkSession.implicits._

    ds.flatMap{ is =>
      val (urlType, requiredTokenMap) = urlParser(is.url, getConfig)

      urlType match {
        case x if urlType.contains("practice") => mapDistinctAttributes(is, requiredTokenMap, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF)
        case x if urlType.contains("test") => mapDistinctAttributes(is, requiredTokenMap, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF)
        case x if urlType.contains("study") => mapAllContentAttributes(is, requiredTokenMap, contentCovList)
        case _ => List(UrlMappedSchema(is.user_id, is.url, is.timestamp, is.date, is.year, is.month, is.week, "", "", "", "", "", "", "", "", "", "", "", ""))
      }
    }
  }

  // -- Test Case
//  val testRow = InputSchema("01",
//    "https://www.embibe.com/engineering/practice/solve/jee/mathematics/algebra/probability/session?fltr=u-hard",
//    "", Date.valueOf("2018-01-01"), 2018, 1, 1)
//  val testTokenMap = Map("goal" -> "engineering", "exam" -> "jee main", "subject" -> "physics", "unit" -> "modern physics and optics")
//  mapDistinctAttributes(testRow, testTokenMap, goalCOVDF, examCOVDF, subjectCOVDF, unitCOVDF, chapterCOVDF)
  // --

  def mapDistinctAttributes(
                             row: InputSchema,
                             tokenMap: Map[String, String],
                             goalCOVDF: Broadcast[Map[String, Map[String, String]]],
                             examCOVDF: Broadcast[Map[String, Map[String, String]]],
                             subjectCOVDF: Broadcast[Map[String, Map[String, String]]],
                             unitCOVDF: Broadcast[Map[String, Map[String, String]]],
                             chapterCOVDF: Broadcast[Map[String, Map[String, String]]]
                           ): List[UrlMappedSchema] = {
    // Goal
    val goalTokenNameOption = tokenMap.get("goal")
    val (goalName, goalCode) = getDistinctTokenCode(goalTokenNameOption, goalCOVDF)
    // Exam
    val examTokenNameOption = tokenMap.get("exam")
    val (examName, examCode) = getDistinctTokenCode(examTokenNameOption, examCOVDF)
    // Subject
    val subjectTokenNameOption = tokenMap.get("subject")
    val (subjectName, subjectCode) = getDistinctTokenCode(subjectTokenNameOption, subjectCOVDF)
    // Unit
    val unitTokenNameOption = tokenMap.get("unit")
    val (unitName, unitCode) = getDistinctTokenCode(unitTokenNameOption, unitCOVDF)
    // Chapter
    val chapterTokenNameOption = tokenMap.get("chapter")
    val (chapterName, chapterCode) = getDistinctTokenCode(chapterTokenNameOption, chapterCOVDF)
    // Concept
    val (conceptName, conceptCode) = ("", "")

    List(
      UrlMappedSchema(
        user_id = row.user_id,
        url = row.url,
        timestamp = row.timestamp,
        date = row.date,
        year = row.year,
        month = row.month,
        week = row.week,
        goal_code =  goalCode,
        goal_name = goalName,
        exam_code = examCode,
        exam_name = examName,
        subject_code = subjectCode,
        subject_name = subjectName,
        unit_code = unitCode,
        unit_name = unitName,
        chapter_code = chapterCode,
        chapter_name = chapterName,
        concept_code = conceptCode,
        concept_name = conceptName
      )
    )
  }

  def getDistinctTokenCode(tokenNameOption: Option[String], tokenNameCodeMapBroadcast: Broadcast[Map[String, Map[String, String]]]) = {

    val (tokenName, tokenCode) = if(tokenNameOption.isDefined && tokenNameOption.get != null && tokenNameOption.nonEmpty) {
      val tokenNameValue = tokenNameOption.get
      val tokenNameCodeMapValue = tokenNameCodeMapBroadcast.value
      val tokenMapOption = tokenNameCodeMapValue.get(tokenNameValue)
      if(tokenMapOption.isDefined) {
        (tokenNameValue, tokenMapOption.get("codeColumn"))
      } else {
        (tokenNameValue, "")
      }
    } else {
      ("", "")
    }

    (tokenName, tokenCode)
  }

  def mapAllContentAttributes(
                               row: InputSchema,
                               tokenMap: Map[String, String],
                               contentCovList: Broadcast[scala.List[CovInputSchema]]
                             ): List[UrlMappedSchema] = {
    val contentCovListValue = contentCovList.value
    val entityCodeOption = tokenMap.get("entity-code")

    val filterCovData = if(entityCodeOption.isDefined){
      val entityCode = entityCodeOption.get
      contentCovListValue.filter{ cov =>
        if(cov.content_code != null && cov.content_code.nonEmpty){
          cov.content_code.toLowerCase == entityCode
        } else {
          false
        }
      }
    } else {
      List.empty[CovInputSchema]
    }
    if(filterCovData.isEmpty){
      List(
          UrlMappedSchema(
          user_id = row.user_id,
          url = row.url,
          timestamp = row.timestamp,
          date = row.date,
          year = row.year,
          month = row.month,
          week = row.week,
          goal_code = "",
          goal_name = "",
          exam_code = "",
          exam_name = "",
          subject_code = "",
          subject_name = "",
          unit_code = "",
          unit_name = "",
          chapter_code = "",
          chapter_name = "",
          concept_code = "",
          concept_name = ""
        )
      )
    }
    else {
      filterCovData.map { cov =>
        UrlMappedSchema(
          user_id = row.user_id,
          url = row.url,
          timestamp = row.timestamp,
          date = row.date,
          year = row.year,
          month = row.month,
          week = row.week,
          goal_code = cov.goal_code,
          goal_name = cov.goal_name,
          exam_code = cov.exam_code,
          exam_name = cov.exam_name,
          subject_code = cov.subject_code,
          subject_name = cov.subject_name,
          unit_code = cov.unit_code,
          unit_name = cov.unit_name,
          chapter_code = cov.chapter_code,
          chapter_name = cov.chapter_name,
          concept_code = cov.concept_code,
          concept_name = cov.concept_name
        )
      }
    }
  }

//  // -- Test Case
//
//  val urlTestList = List("/engineering/practice/solve/jee/mathematics/algebra/probability/session?fltr=u-hard",
//    "/medical/practice/solve/cet-aipmt/physics/mechanics-1/session",
//    "/foundation-10/practice/solve/10th-foundation/session?fltr=u-hard",
//    "/medical/practice/solve",
//    "/engineering/test/jee-main/chapterwise-test/physics/ray-optics",
//    "/engineering/test/jee-main/chapterwise-test/physics/ray-optics/paper",
//    "/engineering/test/jee-main/part-test/physics/modern-physics-and-optics/",
//    "/engineering/test/jee-main/part-test/physics/modern-physics-and-optics/paper",
//    "/engineering/test/bitsat/full-test/test-1/",
//    "/engineering/test/bitsat/full-test/test-1/paper",
//    "/bank/test/bank-po-prelims/previous-year-test/sbi-po-prelims-conducted-on-09-07-2016/",
//    "/bank/test/bank-po-prelims/previous-year-test/sbi-po-prelims-conducted-on-09-07-2016/paper",
//    "/engineering/test/jee-main/previous-year-test/jee-mains-2016---3rd-april--offline",
//    "/engineering/test/jee-main/11th-revision/revision-test-for-class-11-syllabus-jee-main/paper",
//    "/engineering/test/jee-main/mini-test",
//    "/medical/test/aipmt/mini-test/paper",
//    "/study/alkane-concept?entity_code=KTAA01",
//    "/study/redox-reactions-and-volumetric-analysis-chapter?entity_code=ch211",
//    ""
//  )
//
//  urlTestList.map(x => urlParser(x, getConfig))
//
//  urlParser("https://www.embibe.com/", getConfig)
//  urlParser("https://www.embibe.com/search?q=Cell+EMF+=+Difference+in+reduction+potentials+of+cathode+&amp;+anode", getConfig)
//
//  // --

  def urlParser(url: String, config: Config) = {
    val urlParserConfig = config.getConfig("url_parser")
    val invalidUrlList = urlParserConfig.getStringList("invalid_url_list").asScala.toList

    if(url != null && url.nonEmpty && !invalidUrlList.contains(url)) {
      val prefixStripUrl = url.trim.toLowerCase.stripPrefix("https://")
      val urlParserRuleList = urlParserConfig.getConfigList("rules").asScala.toList

      val urlParserRuleConfig = urlParserRuleList.find { parserConfig =>
        val separatorList = parserConfig.getStringList("token_separator_list").asScala.map(_.charAt(0)).toArray
        val tokenizedUrl = prefixStripUrl.split(separatorList).toList.tail
        val identifierConfigList = parserConfig.getConfigList("identifier").asScala.toList
        val bool = identifierConfigList.map { identifierConfig =>
          // removing `<?>.embibe.com`
          val identifierTokenPos = identifierConfig.getInt("token_id") - 1
          val identifierTokenVal = identifierConfig.getString("value")
          if (tokenizedUrl.nonEmpty && tokenizedUrl.length > identifierTokenPos && tokenizedUrl(identifierTokenPos) == identifierTokenVal) true else false
        }.reduce(_ & _)
        bool
      }

      val formatToken = (token: String) => token.trim.toLowerCase.replace("-", " ")

      if (urlParserRuleConfig.isDefined) {
        val parserConfig = urlParserRuleConfig.get
        val separatorList = parserConfig.getStringList("token_separator_list").asScala.map(_.charAt(0)).toArray
        val tokenSequence = parserConfig.getStringList("token_sequence").asScala.toList
        val requiredTokens = parserConfig.getStringList("tokens_required").asScala.toList
        val parserName = parserConfig.getString("parser_name")

        val tokenizedUrl = prefixStripUrl.split(separatorList).toList.tail
        val zippedTokenMap = tokenSequence.zip(tokenizedUrl).toMap
        val tokenMap = requiredTokens.map(token => (token, formatToken(zippedTokenMap.getOrElse(token, "")))).toMap
        (parserName, tokenMap)
      } else {
        ("_dummy", Map.empty[String, String])
      }
    } else {
      ("_dummy", Map.empty[String, String])
    }
  }


  def getDistinctUserId(ds: Dataset[UrlMappedSchema]): Dataset[IntermediateSchema] = {
    import ds.sparkSession.implicits._

    val distinctDF = ds
      .groupBy(col("year"), col("month"))
      .agg(collect_set(col("user_id")).as("user_id_list"))
      .orderBy(col("year"), col("month"))

    distinctDF.as[IntermediateSchema]
  }

  def sol(ds: Dataset[IntermediateSchema], jobTimestamp: Long): Dataset[OutputSchema] = {
    import ds.sparkSession.implicits._

    val pivotYear = 2018
    val pivotConfigMonth = 4
    val endConfigMonth = 10

    val unionDS = (pivotConfigMonth to endConfigMonth).toList.map{ pivotMonth =>
      val pivotRowFilterList = ds.filter(is=> is.year == pivotYear && is.month == pivotMonth).collect()
      val pivotRowEmptyBoolean = pivotRowFilterList.isEmpty
      lazy val pivotRow = pivotRowFilterList.head

      val outputDS = ds.flatMap { is =>
        if (!pivotRowEmptyBoolean && is.month >= pivotMonth) {
          val pivotUserList = pivotRow.user_id_list
          val pivotDistinctUserCount = pivotUserList.length
          val intersectionUserList = pivotUserList.intersect(is.user_id_list)
          val intersectionCount = intersectionUserList.length
          val intersectionPercentage = (intersectionCount.toDouble / pivotDistinctUserCount) * 100
          Some(
            OutputSchema(
              pivotYear,
              pivotMonth,
              is.year,
              is.month,
              pivotDistinctUserCount,
              intersectionCount,
              intersectionPercentage,
              jobTimestamp
            )
          )
        } else {
          None
        }
      }
      outputDS
    }.reduce(_.unionByName(_))

    unionDS
  }

//  def filterConfigParser(ds: Dataset[CovInputSchema], config: Config) = {
//    if(config.hasPath("filter")) {
//      val filterConfig = config.getConfig("filter")
//
//      val goalFilteDS = if(filterConfig.hasPath("goal")){
//        val advancedFilterConfig = filterConfig.getConfig("goal")
//        testFilterConfig(advancedFilterConfig, "goal")
//        getGoalFilterDS(ds, advancedFilterConfig)
//      } else {
//        ds
//      }
//
//      val examFilteDS = if(filterConfig.hasPath("exam")){
//        val advancedFilterConfig = filterConfig.getConfig("exam")
//        testFilterConfig(advancedFilterConfig, "exam")
//        getExamFilterDS(goalFilteDS, advancedFilterConfig)
//      } else {
//        goalFilteDS
//      }
//
//      val subjectFilteDS = if(filterConfig.hasPath("subject")){
//        val advancedFilterConfig = filterConfig.getConfig("subject")
//        testFilterConfig(advancedFilterConfig, "subject")
//        getExamFilterDS(examFilteDS, advancedFilterConfig)
//      } else {
//        examFilteDS
//      }
//
//      val unitFilteDS = if(filterConfig.hasPath("unit")){
//        val advancedFilterConfig = filterConfig.getConfig("exam")
//        testFilterConfig(advancedFilterConfig, "exam")
//        getExamFilterDS(subjectFilteDS, advancedFilterConfig)
//      } else {
//        subjectFilteDS
//      }
//
//      val chapterFilteDS = if(filterConfig.hasPath("chapter")){
//        val advancedFilterConfig = filterConfig.getConfig("exam")
//        testFilterConfig(advancedFilterConfig, "exam")
//        getExamFilterDS(unitFilteDS, advancedFilterConfig)
//      } else {
//        unitFilteDS
//      }
//
//      val conceptFilteDS = if(filterConfig.hasPath("concept")){
//        val advancedFilterConfig = filterConfig.getConfig("exam")
//        testFilterConfig(advancedFilterConfig, "exam")
//        getExamFilterDS(chapterFilteDS, advancedFilterConfig)
//      } else {
//        chapterFilteDS
//      }
//
//      conceptFilteDS
//
//    } else {
//      ds
//    }
//  }
//
//  def testFilterConfig(config: Config, filterName: String): Unit = {
//    if(
//      config.hasPath("list") &&
//      ! config.getConfigList("list").isEmpty &&
//      config.hasPath("type") &&
//      ! config.getString("type").isEmpty
//    ){
//      throw new IllegalArgumentException(s"Advanced Filter Config Issue - Please check $filterName filter config once")
//    }
//  }
//
//  def getGoalFilterDS = ???
//
//  def getExamFilterDS = ???
//
//  def getSubjectFilterDS = ???
//
//  def getUnitFilterDS = ???
//
//  def getChapterFilterDS = ???
//
//  def getConceptFilterDS = ???


  // ---------------- Static --------------------


  def getConfig: Config = {
    val urlParserString =
      """
        |{
        |  "url_parser" : {
        |    "invalid_url_list" : [
        |      "https://www.embibe.com/",
        |      "https://www.embibe.com/404",
        |      "https://www.embibe.com/ai",
        |      "https://www.embibe.com/landing",
        |      "https://www.embibe.com/rankup",
        |      "https://www.embibe.com/search"
        |    ],
        |    "rules": [
        |      {
        |        "parser_name": "practice"
        |        "identifier": [
        |          {
        |            "token_id": 2,
        |            "value": "practice"
        |          }
        |        ],
        |        "token_separator_list": ["/"],
        |        "token_sequence": ["goal", "practice", "solve", "exam", "subject", "unit", "chapter"],
        |        "tokens_required": ["goal", "exam", "subject", "unit", "chapter"]
        |      },
        |      {
        |        "parser_name": "chapterwise-test"
        |        "identifier": [
        |          {
        |            "token_id": 2,
        |            "value": "test"
        |          },
        |          {
        |            "token_id": 4,
        |            "value": "chapterwise-test"
        |          }
        |        ],
        |        "token_separator_list": ["/"],
        |        "token_sequence": ["goal", "test", "exam", "chapterwise-test", "subject", "chapter"],
        |        "tokens_required": ["goal", "exam", "subject", "chapter"]
        |      },
        |      {
        |        "parser_name": "part-test"
        |        "identifier": [
        |          {
        |            "token_id": 2,
        |            "value": "test"
        |          },
        |          {
        |            "token_id": 4,
        |            "value": "part-test"
        |          }
        |        ],
        |        "token_separator_list": ["/"],
        |        "token_sequence": ["goal", "test", "exam", "chapterwise-test", "subject", "unit"],
        |        "tokens_required": ["goal", "exam", "subject", "unit"]
        |      },
        |      {
        |        "parser_name": "other-test"
        |        "identifier": [
        |          {
        |            "token_id": 2,
        |            "value": "test"
        |          }
        |        ],
        |        "token_separator_list": ["/"],
        |        "token_sequence": ["goal", "test", "exam", "test-type", "test-type"],
        |        "tokens_required": ["goal", "exam"]
        |      },
        |      {
        |        "parser_name": "study"
        |        "identifier": [
        |          {
        |            "token_id": 1,
        |            "value": "study"
        |          }
        |        ],
        |        "token_separator_list": ["/", "?", "="],
        |        "token_sequence": ["study", "entity-name-type", "entity_code", "entity-code"],
        |        "tokens_required": ["entity-code"]
        |      }
        |    ]
        |  }
        |}
      """.stripMargin

    ConfigFactory.parseString(urlParserString)
  }


  val covSchema: StructType = {
    val rootSchema = StructType(
      Seq(
        StructField("content_code", StringType),
        StructField("goal_code", StringType),
        StructField("goal_name", StringType),
        StructField("exam_code", StringType),
        StructField("exam_name", StringType),
        StructField("subject_code", StringType),
        StructField("subject_name", StringType),
        StructField("unit_code", StringType),
        StructField("unit_name", StringType),
        StructField("chapter_code", StringType),
        StructField("chapter_name", StringType),
        StructField("concept_code", StringType),
        StructField("concept_name", StringType)
      )
    )
    rootSchema
  }

  val inputSchema: StructType = {
    val propertiesSchema = StructType(
      Seq(
        StructField("user_id", StringType)
      )
    )
    val pageSchema = StructType(
      Seq(
        StructField("url", StringType),
        StructField("path", StringType)
      )
    )
    val contextSchema = StructType(
      Seq(
        StructField("page", pageSchema)
      )
    )
    val rootSchema = StructType(
      Seq(
        StructField("userId", StringType),
        StructField("timestamp", StringType),
        StructField("properties", propertiesSchema),
        StructField("context", contextSchema)
      )
    )
    rootSchema
  }
}



case class InputSchema(
                        user_id: String,
                        url: String,
                        path: String,
                        timestamp: String,
                        date: Date,
                        year: Int,
                        month: Int,
                        week: Int
                      )

case class IntermediateSchema(
                               year: Int,
                               month: Int,
                               user_id_list: List[String]
                             )

case class OutputSchema(
                         pivotYear: Int,
                         pivotMonth: Int,
                         otherYear: Int,
                         otherMonth: Int,
                         pivotCount: Int,
                         retentionCount: Int,
                         retentionPercentage: Double,
                         executionTimestamp: Long
                       )

case class OutputSchema_Refactor(
                                  pivotYear: Int,
                                  pivot: Int,
                                  pivotView: String,
                                  otherYear: Int,
                                  other: Int,
                                  otherView: String,
                                  analysisType: String,
                                  advancedFilter: String,
                                  pivotCount: Int,
                                  retentionCount: Int,
                                  retentionPercentage: Double,
                                  executionTimestamp: Long,
                                  sno: String
                                )

case class CovInputSchema(
                           content_code: String,
                           goal_code: String,
                           goal_name: String,
                           exam_code: String,
                           exam_name: String,
                           subject_code: String,
                           subject_name: String,
                           unit_code: String,
                           unit_name: String,
                           chapter_code: String,
                           chapter_name: String,
                           concept_code: String,
                           concept_name: String
                         )

case class UrlMappedSchema(
                            user_id: String,
                            url: String,
                            timestamp: String,
                            date: Date,
                            year: Int,
                            month: Int,
                            week: Int,
                            goal_code: String,
                            goal_name: String,
                            exam_code: String,
                            exam_name: String,
                            subject_code: String,
                            subject_name: String,
                            unit_code: String,
                            unit_name: String,
                            chapter_code: String,
                            chapter_name: String,
                            concept_code: String,
                            concept_name: String
                          )

//case class goalDbPullSchema(goal_code:String, goal_name: String, goal_slug: String)
//case class examDbPullSchema(exam_code:String, exam_name: String, exam_slug: String, goal_code: String)
//case class subjectDbPullSchema(subject_code:String, subject_name: String, subject_slug: String, subject_test_xpath_csv: String, subject_practice_xpath_csv: String, exam_code: String)
//case class unitDbPullSchema(unit_code:String, unit_name: String, unit_slug: String, unit_test_xpath_csv: String, unit_practice_xpath_csv: String, subject_code: String)
//case class chapterDbPullSchema(chapter_code:String, chapter_name: String, chapter_slug: String, chapter_test_xpath_csv: String, chapter_practice_xpath_csv: String, unit_code: String)
//case class conceptDbPullSchema(concept_code:String, concept_name: String, concept_relevant_exams: String)
